﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextRecalculate(object sender, TextChangedEventArgs e)
        {
            Tempconv();
        }

        private void Recalculate(object sender, RoutedEventArgs e)
        {
            Tempconv();
        }

        private void Tempconv()
        {
            double input;
            if (double.TryParse(InputText.Text, out input))
            {
                if (InputCelcius.IsChecked == true)
                {
                    if (OutputCelcius.IsChecked == true)
                    {
                        Outputlbl.Content = string.Format("{0:0.0} C", input);
                    }
                    else if (OutputFarenheit.IsChecked == true)
                    {
                        input = 1.8 * input + 32;
                        Outputlbl.Content = string.Format("{0:0.0} F", input);
                    }
                    else if (OutputKelvin.IsChecked == true)
                    {
                        input = input + 273.15;
                        Outputlbl.Content = string.Format("{0:0.0} K", input);
                    }
                }
                else if (InputFarenheit.IsChecked == true)
                {
                    if (OutputCelcius.IsChecked == true)
                    {
                        input = (input - 32) / 1.8;
                        Outputlbl.Content = string.Format("{0:0.0} C", input);
                    }
                    else if (OutputFarenheit.IsChecked == true)
                    {
                        Outputlbl.Content = string.Format("{0:0.0} F", input);
                    }
                    else if (OutputKelvin.IsChecked == true)
                    {
                        input = (input - 32) * 5 / 9 + 273.15;
                        Outputlbl.Content = string.Format("{0:0.0} K", input);
                    }

                }
                else if (InputKelvin.IsChecked == true)
                {
                    if (OutputCelcius.IsChecked == true)
                    {
                        input = input - 273.15;
                        Outputlbl.Content = string.Format("{0:0.0} C", input);
                    }
                    else if (OutputFarenheit.IsChecked == true)
                    {
                        input = (input - 273.15) * 9 / 5 + 32;
                        Outputlbl.Content = string.Format("{0:0.0} F", input);
                    }
                    else if (OutputKelvin.IsChecked == true)
                    {
                        Outputlbl.Content = string.Format("{0:0.0} K", input);
                    }
                }
            }

            
        }
    }
}
